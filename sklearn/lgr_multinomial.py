#!/usr/bin/env python3
"""
logistic Regression for Multivariable/Multiclass problems

Todo: Need to cleanup & simplify this script. Perhaps expand on the
model section to have it swap out for other types.

setup/install memcached & ipyparallel
1) Setup the python environment
  pyenv install anaconda3-2019.07
  pyenv virtualenv anaconda3-2019.07 <somename>
  pyenv shell <somename> 
  pip install -r lgr-requirements.txt

2) Start the cluster
  ipcluster start -n 4
3) Train & tune from stems csv file. This splits 80/20 from test file.
  ./lgr_multinomial.py \
      --train-file ../data/stems_length_limited_train.csv \
      --rebuild-model --rebuild-validations
4) Build predictions table
  ./lgr_multinomial.py \
      --train-file ../data/stems_length_limited_train.csv \
      --test-file ../data/stems_length_limited_test.csv \
      --skip-pretest --rebuild-predictions
5) Save csv file..
   ./lgr_multinomial.py \
      --train-file ../data/stems_length_limited_train.csv \
      --test-file ../data/stems_length_limited_test.csv \
      --skip-pretest

"""

from argparse import ArgumentParser
import collections
import copy
import csv
import datetime as dt
from functools import partial
import h5py
import io
import itertools
import ipyparallel as ipl
from ipyparallel import Client
from ipyparallel.joblib import IPythonParallelBackend
import json
import math
import multiprocessing as mp
import numpy as np
import pandas as pd
from pathlib import Path
import pathlib as plib
import pickle
import pprint
from pymemcache.client import base
import random
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, OrdinalEncoder, MaxAbsScaler
# from sklearn.externals.joblib import parallel_backend  ## figure out dask?
from joblib import Parallel, parallel_backend, register_parallel_backend
# from sklearn.externals.joblib import Parallel, parallel_backend,register_parallel_backend
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn import linear_model as lm
from sklearn.tree import DecisionTreeRegressor
from scipy import sparse
import sys
import tables
from tabulate import tabulate
import timeit
import time


pp = pprint.PrettyPrinter(indent=4)
## setup parallel tasks for sklearn + ipyparallel (should also work with Dask eventually)
try:
    c = Client()
except IOError as e:
    print("You need to start ipcluster: $> ipcluster start -n <cpu count>")
    raise e
except ipl.error.TimeoutError as e:
    print("You need to start ipcluster: $> ipcluster start -n <cpu count>")
    raise e

bview = c.load_balanced_view()
register_parallel_backend('ipyparallel', lambda : IPythonParallelBackend(view=bview))

MAXIMUM_PREDICTIONS=200


def query_yes_no(question, default='yes'):
    """
    query_yes_no sourced from https://code.activatestate.com/recipes/577058
    """
    valid = {
        "yes":"yes", "y":"yes", "ye":"yes",
        "no":"no", "n":"no"
    }
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while 1:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return default
        elif choice in valid.keys():
            return valid[choice]
        else:
            sys.stdout.write(
                "Please respond with 'yes' or 'no' "
                "(or 'y' or 'n').\n"
            )


def load_dataframe(filepath):
    """
    returns a dataframe from a csv filepath
    """
    t1 = timeit.default_timer()
    _filepath = Path(filepath).resolve()
    if(_filepath.is_file()):
        dfret = pd.read_csv(_filepath, index_col=0)
        t2 = timeit.default_timer()
        t2 = timeit.default_timer()
        dtime = (t2 - t1)
        print("CSV load time: {0:.2f} secs".format(dtime))
        return dfret
    else:
        print("Error: '{}' is not a file!".format(_filepath))
        sys.exit(1)


def split_on(*args, **kwargs):
    """
    Given a dataframe and the column, runs the str.split
    on each row using the split_char. returns the modified dataframe.
    """
    colnam = args[0]  # column name
    df_data = args[1]  # our dataframe
    split_char = kwargs.get('split_char', ',')
    for index, row in df_data.iterrows():
        df_data.at[index, colnam] = row[colnam].split(split_char)
    return df_data


def preprocess_split_topics(df):
    """
    convert topics column into a list of topics from a string.
    """
    print("Spliting topics...")
    dfret = parallelize_process(
        df,  # pandas dataframe
        split_on,  # parallelized function
        'topics',  # dataframe column
        split_char=',',  # character to split on
    )
    return dfret


def parallelize_process(input_df, func, *args, **kwargs):
    """
    Proof of concept, passing arguments to our map/cpu pool
    Dask? maybe...
    """
    t1 = timeit.default_timer()
    input_df_clone = input_df.copy()
    memclient = base.Client(('localhost', 11211))
    data_partitions = json.loads(memclient.get('data_partitions'))
    df_split = np.array_split(input_df_clone, data_partitions)
    call_func = partial(func, *args, **kwargs)
    pool = mp.Pool(json.loads(memclient.get('cpu_count')))
    df = pd.concat(pool.map(call_func, df_split))
    pool.close()
    pool.join()
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("Parallel task took: {0:.2f} secs".format(dtime))
    return df


def preprocess_training_class_label_encoder(df, class_colname='topics', clip_at=-1):
    """
    Given a list of topics, generate a label encoder
    Note: clip_at=10 means if <10 examples for topic, drop topic word. -1 is no clipping
    Returned classes_lb_enc will be limited to the 'kept_classes' list.
    """
    t1 = timeit.default_timer()
    class_odict = collections.OrderedDict()
    dfc = df.copy()
    class_keys = list(dfc[class_colname].apply(tuple).value_counts().keys().tolist())
    class_counts = list(df[class_colname].apply(tuple).value_counts().tolist())
    ## gather counts for each topic key (include frequencies to push the more common to the start)
    ## Can use this to build histograph and filter out low occuring entries.
    for i, classes in enumerate(class_keys):
        for c in classes:
            class_odict[c] = class_odict.get(c, 0) + class_counts[i]
    # Sort our classes based on examples in the dataframe.
    sorted_odict = collections.OrderedDict(sorted(class_odict.items(), key=lambda kv: kv[1], reverse=True))
    ## Make our dataframe include columns for each topic
    classes = list(sorted_odict.keys())
    
    print("Total training classes: {}".format(len(sorted_odict.keys())))
    # pp.pprint(sorted_odict)
    ## Looking at a histograph, 800-1750 count frequencies are most common
    if(clip_at != -1):
        sorted_odict = collections.OrderedDict(
            [ (k,v) for (k,v) in list(sorted_odict.items()) if v >= clip_at ]
        )
        print("Kept classes: {} / clipped at: {}".format(len(sorted_odict), clip_at))
    kept_classes = list(sorted_odict.keys())
    # Create labeler for our classes list "Topic name" -> integer encoder
    classes_lb_enc = LabelEncoder()
    classes_lb_enc.fit(np.asarray(kept_classes))  ## Was keep_topic_list -> convert this to all!!
    t2 = timeit.default_timer()
    dtime = (t2 - t1) / 60
    print("Generating Label Encoder time: {0:.2f} secs".format(dtime))
    return classes, classes_lb_enc, kept_classes


def decompose_on(*args, **kwargs):
    """
    Using a label encoder, converts the "topics" column into a list of integers. Then breaks
    up each topic into a new row to unpack data for the Logistical Analysis step.
    """
    colname = args[0]  # Column name
    classes_lb_enc = args[1]  # LabelEncoder
    kept_classes = args[2]  # classes we can "model"
    df_data = args[3]  # The dataframe

    new_dict_data = { x: [] for x in list(df_data.columns) if x != colname}
    new_dict_data['class_count'] = []  ## used to stash number of classes for our multilabel row
    new_dict_data['class'] = []
    for index, row in df_data.iterrows():
        class_count = 0
        class_list = [c for c in row[colname] if c in kept_classes]
        # Skip this row if our class list is empty
        if(len(class_list) <= 0):
            # sys.stdout.write("-")
            next
        # Attempt to translate text classes into numeric list with LabelEncoder
        try:
            # This will convert our "topics" column data into a list of numbers
            row_classes = classes_lb_enc.transform(class_list)
        except ValueError as e:
            ## ignore words missing from our LabelEncoder
            # sys.stdout.write("_")
            row_classes = []
        else:
            # sys.stdout.write(".")
            class_count = len(row_classes)
            # only insert those classes that are in the LabelEncoder
            for row_class in row_classes:
                # for each element
                for col in list(row.index):
                    if col != colname:
                        new_dict_data[col].append(row[col])
                new_dict_data['class_count'].append(class_count)
                new_dict_data['class'].append(row_class)
    return pd.DataFrame(new_dict_data, columns=new_dict_data.keys())


def preprocess_decompose_classes(df, classes_lb_enc, kept_classes, colname='topics'):
    """
    Given a class column name with a list, decompose by cloning
    each classes 'row' into a new row for each class found in that list.
    """
    print("Decomposing topics...")
    dfret = parallelize_process(
        df,  # pandas dataframe to process
        decompose_on,  # parallelized function
        colname,
        classes_lb_enc,
        kept_classes
    )
    return dfret


def preprocess_tfid_transform(df, colname='tokens'):
    """
    Returns the count vec & tfid transformer
    """
    t1 = timeit.default_timer()
    cnt_vec = CountVectorizer(stop_words='english', binary='true')
    cnt_vec.fit(df[colname].values)
    tfidf_transformer_tk = TfidfTransformer().fit(cnt_vec.transform(df[colname]))
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("Transformer time: {0:.2f} secs".format(dtime))
    return cnt_vec, tfidf_transformer_tk


def train_model(df, X_train, y_train, multi_class='ovr'):
    t1 = timeit.default_timer()
    clf = None
    print("Training model input Dimensions")
    print("X : {}".format(X_train.shape))
    print("y : {}".format(len(y_train)))
    with parallel_backend('ipyparallel'):
        # Sadly the only "non-parallelizable" mode appears to be the multinomial
        # However, it is very very fast: ~8mins for 1k iterations on an old i7 macbook
        # clf = lm.LogisticRegression(
        #     # penalty='l2',
        #     # solver='sag',  # Good for large data sets
        #     solver='newton-cg',
        #     # max_iter=1000,  # max number of iterations to converge on solution / default (lower == faster)
        #     # random_state=89,  # Random seed
        #     multi_class='multinomial',  # not sure.. multinomial vs ovr?
        #     # multi_class=multi_class,  # ovr is 25 sec vs 9mins for multinomial...
        #     # n_jobs=-1,
        # )
        #SGD Classifier
        clf = lm.SGDClassifier(
            loss='log',
            penalty='elasticnet',
            max_iter=3,
            alpha=0.000005
        )

        clf.fit(X_train, y_train)
    t2 = timeit.default_timer()
    dtime = (t2 - t1) / 60
    print("Training time: {0:.2f} minutes".format(dtime))
    return clf


def predictor_func(*args):
    """
    Specialized "predictor" function for parallelize_predictions
    """
    predictions = []
    clf = args[1]
    for i, (ix, x_row) in enumerate(args[0]):
        cmp_prob = clf.predict_proba(x_row)[0]
        sorted_guesses = sorted(
        [ (j, jprb) for j, jprb in enumerate(cmp_prob) ],
            key=lambda tup: tup[1],
            reverse=True
        )
        # if(ix == 45103):
        #     pp.pprint(sorted_guesses)
        # limit MAXIMUM_PREDICTIONS, i.e. ~200
        # predictions.append((ix, sorted_guesses[:MAXIMUM_PREDICTIONS]))
        predictions.append((ix, sorted_guesses))
    return predictions


def parallelize_predictions(clf, X, Xindex, data_partitions, cpu_count):
    """
    Similar to parallel process but operate on a list..
    """
    print("parallelize_predictions()")
    predictions = []
    t1 = timeit.default_timer()

    input_dataset = list(zip(Xindex, X[:, :]))
    input_size = len(input_dataset)
    n = int(math.ceil(input_size / data_partitions))
    split_dset = [input_dataset[i * n:(i + 1) * n] for i in range((input_size + n - 1) // n )]
    print("split set size: {}".format(len(split_dset)))
    with mp.Pool(cpu_count) as pool:
        results = pool.starmap(predictor_func, zip(split_dset, itertools.repeat(clf)))
        pool.close()
        pool.join()
    predictions = list(itertools.chain.from_iterable(results))
    print("total predictions made: {}".format(len(predictions)))
    t2 = timeit.default_timer()
    dtime = (t2 - t1) / 60
    print("Predictions took: {0:.2f} mins".format(dtime))
    return predictions


def solve_predictions(*args, **kwargs):
    """
    Parallized to find predictions
    """
    clf = args[0]
    df_data = args[1]
    return df_data


def load_data(fobj, data_name):
    """
    Returns a data blob from our hdf5 dataset
    """
    print("[{}] <- disk".format(data_name))
    if((fobj is not None) and data_name in fobj.keys()):
        raw_data = fobj.get(data_name)
        io_bytes = io.BytesIO()
        io_bytes.write(raw_data[0:])
        io_bytes.seek(0)
        data_obj = pickle.load(io_bytes)
        del io_bytes
        del raw_data
        return data_obj
    return None


def store_data(fobj, data_name, data):
    """
    Pickle then save a python object to our hdf5 datastore
    """
    print("[{}] -> disk".format(data_name))
    bytes_io = io.BytesIO()
    pickle.dump(data, bytes_io, protocol=pickle.HIGHEST_PROTOCOL)
    if(data_name in fobj.keys()):
        # TODO: this needs to trigger "h5repack" to shrink our hdf5 file.
        del fobj[data_name]
    dset = fobj.create_dataset(data_name, data=np.void(bytes_io.getbuffer()))
    dset.attrs[u'size'] = bytes_io.getbuffer().nbytes
    del bytes_io
    del dset


def restore_models(hdf5_filepath):
    """
    If the file exists, attempts to load in data
    """
    print("Reading models in file from: '{}'".format(str(hdf5_filepath)))
    t1 = timeit.default_timer()
    f = h5py.File(str(hdf5_filepath), 'r')
    scaler = load_data(f, u'scaler')
    clf = load_data(f, u'model')
    median_y_len = load_data(f, u'median_y_len')
    if(clf is None):
        raise Exception("No model in dataset!")
    topics = load_data(f, u'topics')
    if(topics is None):
        raise Exception("No topics in dataset!")
    topics_lb_enc = load_data(f, u'topics_lb_enc')
    if(topics_lb_enc is None):
        raise Exception("No topics label encoder in dataset!")
    cnt_vec = load_data(f, u'cnt_vec')
    if(cnt_vec is None):
        raise Exception("No count vectorizer in dataset!")
    tfidf_transformer_tk = load_data(f, u'tfidf_transformer_tk')
    if(tfidf_transformer_tk is None):
        raise Exception("No TFID transformer in dataset!")
    # y_class_cnt = f['y_class_cnt'][:]
    y = f['y'][:]
    f.close()
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("Load time: {0:.2f} secs".format(dtime))
    return (
        scaler, clf, topics, topics_lb_enc, cnt_vec, 
        tfidf_transformer_tk, 
        # y_class_cnt,
        y, median_y_len
    )


def save_model(
    hdf5_filepath, scaler, clf, topics, topics_lb_enc, cnt_vec,
    tfidf_transformer_tk, 
    # y_class_cnt,
    y, median_y_len):
    """
    Store our model and encoders to disk
    """
    t1 = timeit.default_timer()
    f = h5py.File(str(hdf5_filepath), 'a')
    # Save stuff
    store_data(f, u'scaler', scaler)
    store_data(f, u'model', clf)
    store_data(f, u'topics', topics)
    store_data(f, u'topics_lb_enc', topics_lb_enc)
    store_data(f, u'cnt_vec', cnt_vec)
    store_data(f, u'tfidf_transformer_tk', tfidf_transformer_tk)
    store_data(f, u'median_y_len', median_y_len)
    # print("[y_class_cnt] -> disk")
    # if('y_class_cnt' in f.keys()):
        # del f['y_class_cnt']
    # f.create_dataset("y_class_cnt",  data=y_class_cnt)
    print("[y] -> disk")
    if('y' in f.keys()):
        del f['y']
    f.create_dataset("y", data=y)
    f.close()
    t2 = timeit.default_timer()
    dtime = (t2 - t1) / 60
    print("Save time: {0:.2f} mins".format(dtime))


def store_sparse_matrix(M, name, hdf5_filepath):
    """
    Sourced from: https://stackoverflow.com/questions/11129429/storing-numpy-sparse-matrix-in-hdf5-pytables
    """
    t1 = timeit.default_timer()
    assert(M.__class__ == sparse.csr.csr_matrix), 'M must be a csr matrix!'
    with tables.open_file(hdf5_filepath, 'a') as f:
        for attribute in ('data', 'indices', 'indptr', 'shape'):
            full_name = f'm_{name}_{attribute}'
            # remove previous?
            try:
                n = getattr(f.root, full_name)
                n._f_remove()
            except AttributeError:
                pass
            arr = np.array(getattr(M, attribute))
            atom = tables.Atom.from_dtype(arr.dtype)
            ds = f.create_carray(f.root, full_name, atom, arr.shape)
            ds[:] = arr
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("store_sparse_matrix time: {0:.2f} secs".format(dtime))


def load_sparse_matrix(name, hdf5_filepath):
    """
    Sourced from: https://stackoverflow.com/questions/11129429/storing-numpy-sparse-matrix-in-hdf5-pytables
    """
    t1 = timeit.default_timer()
    with tables.open_file(hdf5_filepath) as f:
        attributes = []
        for attribute in ('data', 'indices', 'indptr', 'shape'):
            attributes.append(getattr(f.root, f'm_{name}_{attribute}').read())
    M = sparse.csr_matrix(tuple(attributes[:3]), shape=attributes[3])
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("load_sparse_matrix time: {0:.2f} secs".format(dtime))
    return M


def hdf5_store_df(df, name, hdf5_filepath):
    """
    Save a pandas dataframe to disk
    Note: Issue with storing lists in column, must 1st convert
    our "topics" list column into a string. Also cannot do this on the
    original passed dataframe :(
    """
    t1 = timeit.default_timer()
    df_clone = df.copy()
    # Convert our topics column into a "string"
    df_clone['topics'] = [','.join(map(str, l)) for l in df_clone['topics']]
    df_clone.to_hdf(hdf5_filepath, name, mode='a', format='table', data_columns=True)
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("store_dataframe save time: {0:.2f} secs".format(dtime))


def hdf5_load_df(name, hdf5_filepath):
    """
    Restore pandas dataframe from hdf5 file
    Note: topics column needs to be 'unpacked'
    """
    t1 = timeit.default_timer()
    df = pd.read_hdf(hdf5_filepath, name)
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("load_dataframe save time: {0:.2f} secs".format(dtime))
    return df


def postprocess_predictions(df):
    """
    Given a dataframe, take predictions and build a Linear Regression model for predicting
    the number of guesses we should make.

    original: postprocess_predictions(pred_df, predictions)
    """
    print("Generate X/y of predictions dataset")
    t1 = timeit.default_timer()
    # X = np.empty(shape=(len(predictions), MAXIMUM_PREDICTIONS))
    # Xr = np.empty(shape=(len(predictions), MAXIMUM_PREDICTIONS-1))
    # Xc = np.empty(shape=(len(predictions), MAXIMUM_PREDICTIONS-1))
    # Xindex = np.empty(shape=(len(predictions), 1))
    # Xindex = []
    y = df.topics.apply(lambda x: len(x)).values
    sumv = 0
    for xv in y:
        sumv += xv
    medianv = sumv / len(y)
    print("Mean: {}".format(medianv))
    # for i, (ix, pred) in enumerate(predictions):
    #     npred = [ p[1] for p in pred ]
    #     rpred = [ abs((x1 - npred[i+1])) for i, x1 in enumerate(npred) if i < (len(npred) - 1) ]
    #     cpred = [ (x1 + npred[i+1])/2 for i, x1 in enumerate(npred) if i < (len(npred) - 1) ]
    #     np.append(X, npred)
    #     np.append(Xr, rpred)
    #     np.append(Xc, cpred)
    #     Xindex.append(ix)
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("preprocess time: {0:.2f} secs".format(dtime))
    # return X, Xr, Xc, Xindex, y
    return medianv


def postprocess_train_linear_reg(X_pred, y):
    """
    Given X/y fit to Linear Regression
    """
    t1 = timeit.default_timer()
    # regr = lm.LinearRegression()
    # regr = DecisionTreeRegressor(max_depth=200)
    regr = lm.LinearRegression()
    # pp.pprint(y)
    regr.fit(X_pred, y)
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("Linear Regression Training time for guessing guesses: {0:.2f} secs".format(dtime))
    return regr


def evalutate_model(df, mean, predictions):
    """

    """
    mean_guessing_match = 0
    perfect_guessing_match = 0
    # probability guessed matching algorithm?
    print("Evaluate Logisitc regression model...")
    for row in df.iterrows():
        print(row)
        break
    # for i, (ix, pred) in enumerate(predictions):
    #     print("::ACTUAL::")
    #     print(tabulate(df.iloc[ix:ix+1], headers=df.columns))
    #     print("::PREDICTED::")
    #     print("{}: {}, {}, {}".format(ix, pred[0][0], pred[1][0], pred[2][0]))


def preprocess_balance_classes(df):
    """
    Balance our classes to be more evenly distributed
    'class' integer representation of a single unique class
    'class_count' weighted higher since this is duplicated class_count times for this 'class'
    """
    print("Balancing Dataset")
    t1 = timeit.default_timer()
    new_dict_data = { 'tokens': [], 'class': []}
    # new_dict_data['index'] = []
    tallies = {}  ## 
    token_pool = {}  # this stores a list of token potentals for each "class"
    for index, row in df.iterrows():
        if row['class'] not in tallies:
            tallies[row['class']] = 1
        else:
            tallies[row['class']] += 1
    
        if row['class'] not in token_pool:
            token_pool[row['class']] = [(row['tokens'], row['class_count'])]
        else:
            token_pool[row['class']].append((row['tokens'], row['class_count']))

        for cl in ['tokens', 'class']:
            new_dict_data[cl].append(row[cl]) 

    sorted_tallies = sorted(tallies.items(), key=lambda x: x[1], reverse=True)
    max_class_cnt = sorted_tallies[0][1]
    
    ## Create a list to random insert new rows
    # (Class Number, )
    new_entry = []
    for i, (class_num, cur_cnt) in enumerate(sorted_tallies):
        # class_num
        # (max_class_cnt - cur_cnt) : add this many
        # print("{}: class({})".format(i, class_num))
        # pp.pprint(token_pool[class_num][0:1])
        
        len_of_pool = len(token_pool[class_num])
        for j in range(max_class_cnt - cur_cnt):
            for k in range(token_pool[class_num][j % len_of_pool][1]):
                new_dict_data['class'].append(class_num)
                new_dict_data['tokens'].append(token_pool[class_num][(j % len_of_pool)][0])
                # print(token_pool[class_num][(j % len_of_pool)][0])    
    print("Max class count: {}".format(max_class_cnt)) ## adjusts all other entries to be similar..
    new_df = pd.DataFrame(new_dict_data, columns=new_dict_data.keys())
    t2 = timeit.default_timer()
    dtime = (t2 - t1)
    print("balance time: {0:.2f} secs".format(dtime))
    return new_df


def main(**kwargs):
    print("Logistic Regression Multivariable-Multiclass classifier...")
    memclient = base.Client(('localhost', 11211))
    cpu_count = mp.cpu_count()
    memclient.set('cpu_count', json.dumps(cpu_count))
    data_partitions = (cpu_count * 2) + 2
    memclient.set('data_partitions', json.dumps(data_partitions))

    if('train_csv_path' in kwargs):
        train_csv_path = kwargs.get('train_csv_path')
        ## 80% training dataset
        if(not kwargs.get('rebuild_model')):
            print("Restoring models...")
            (
                scaler, clf, topics, topics_lb_enc, cnt_vec,
                tfidf_transformer_tk,
                # y_class_cnt,
                y_train, median_y_len
            ) = restore_models(kwargs['hdf5_path'])
            X_train = load_sparse_matrix('X_train', kwargs['hdf5_path'])
            train_df = hdf5_load_df('train_df', kwargs['hdf5_path'])
            train_df = preprocess_split_topics(train_df)
            # pred_df = hdf5_load_df('pred_df', kwargs['hdf5_path'])
            # pred_df = preprocess_split_topics(pred_df)
            test_df = hdf5_load_df('test_df', kwargs['hdf5_path'])
            test_df = preprocess_split_topics(test_df)
            print("  Training set size: {}".format(train_df.shape[0]))
            # print("Prediction set size: {}".format(pred_df.shape[0]))
            print("   Testing set size: {}".format(test_df.shape[0]))
            print("      y length mean: {}".format(median_y_len))
        else:
            print("Generating Model from training data @ {}".format(dt.datetime.now()))
            print("Loading training data: {}".format(train_csv_path))
            input_df = load_dataframe(train_csv_path)
            input_df = input_df.sample(frac=1, random_state=32)
            input_df = preprocess_split_topics(input_df)

            # Split data 80/20  (Training/Test)
            size_of_training_set = input_df.shape[0]
            train_size = int(math.floor(size_of_training_set * 0.8))  # Used to build Logisitic model
            test_size = int(math.floor((size_of_training_set - train_size)))  # Used to validate
            test_size += size_of_training_set - (train_size + test_size) # pad remainder to end of test set
            if(size_of_training_set != (train_size + test_size)):
                raise Exception(
                    "All datasets do not sum right: {} != {} + {}".format(
                        size_of_training_set, train_size, test_size
                    )
                )
            print("  Training set size: {}".format(train_size))
            print("   Testing set size: {}".format(test_size))
            train_df = input_df.head(train_size).copy()
            ## Show last row
            # print(tabulate(train_df[(train_size-1):train_size], headers=train_df.columns))
            if(train_df.shape[0] != train_size):
                raise Exception(
                    "Training data set not correct size! {} != {}".format(
                        train_df.shape[0], train_size
                    )
                )
            test_df = input_df.tail(test_size).copy()
            
            # print(tabulate(test_df[0:1], headers=test_df.columns))
            if(test_df.shape[0] != test_size):
                raise Exception(
                    "Testing data set not correct size! {} != {}".format(
                        test_df.shape[0], test_size
                    )
                )
            # print(tabulate(input_df.iloc[0]))
            # print(tabulate(train_df.iloc[0], headers=train_df.columns))
            print("Calculate mean for number of guesses over dataset")
            tdf = train_df.copy()
            y_len = tdf.topics.apply(lambda x: len(x)).values
            sumv = 0
            for xv in y_len:
                sumv += xv
            median_y_len = sumv / len(y_len)
            print("median_y_len: {}".format(median_y_len))

            hdf5_store_df(train_df, 'train_df', kwargs['hdf5_path'])
            hdf5_store_df(test_df, 'test_df', kwargs['hdf5_path'])
            topics, topics_lb_enc, kept_topics = preprocess_training_class_label_encoder(
                train_df, clip_at=150)  # min 10 / max 315
            print("Total number of kept classes: {}".format(len(kept_topics)))
            # Re-order our training dataset to be flattened so it can work with Logistic Regression Classifier
            print("Original Training row count: {}".format(train_df.shape[0]))
            train_df = preprocess_decompose_classes(train_df, topics_lb_enc, kept_topics)
            print("After decomp row count: {}".format(train_df.shape[0]))
            # Balance our training data for outcomes
            # train_df = preprocess_balance_classes(train_df)
            print("After Balance row count: {}".format(train_df.shape[0]))

            cnt_vec, tfidf_transformer_tk = preprocess_tfid_transform(train_df)
            print("Setup X/y datasets...")
            # this generates a sparse matrix?
            X_train = tfidf_transformer_tk.transform(cnt_vec.transform(train_df['tokens']))
            scaler = MaxAbsScaler()
            scaler.fit(X_train)
            X_train = scaler.transform(X_train)
            # print(tabulate(X_train[0]))
            # y_class_cnt = train_df['class_count'].values
            y_train = train_df['class'].values
            clf = train_model(train_df, X_train, y_train)
            print("Save models to disk...")
            save_model(
                kwargs['hdf5_path'],
                scaler,
                clf,
                topics,
                topics_lb_enc,
                cnt_vec,
                tfidf_transformer_tk,
                # y_class_cnt,
                y_train,
                median_y_len
            )
            print("Saving X_train sparse matrix...")
            t1 = timeit.default_timer()
            store_sparse_matrix(X_train, 'X_train', kwargs['hdf5_path'])
            t2 = timeit.default_timer()
            dtime = (t2 - t1)
            print("Save time: {} secs".format(dtime))

        print("X_train shape: {}".format(X_train.shape))
        print("X_train type: {}".format(type(X_train)))
        print("X_train size: {0:.2f} Mb".format(X_train.data.nbytes / (1024*1024)))
        
        ## 20% validating dataset
        if(kwargs.get('rebuild_validations')):
            print("Building validation dataset")
            X_pretest = tfidf_transformer_tk.transform(cnt_vec.transform(test_df['tokens']))
            X_pretest = scaler.transform(X_pretest)
            Xindex = list(test_df.index)
            print("X_pretest shape: {}".format(X_pretest.shape))
            print("X_pretest type: {}".format(type(X_pretest)))
            print("X_pretest size: {0:.2f} Mb".format(X_pretest.data.nbytes / (1024*1024)))
            validations = parallelize_predictions(clf, X_pretest, Xindex, data_partitions, cpu_count)
            store_sparse_matrix(X_pretest, 'X_pretest', kwargs['hdf5_path'])
            f = h5py.File(str(kwargs['hdf5_path']), 'a')
            store_data(f, u'validations', validations)
            print("Saved validations: {} bytes".format(sys.getsizeof(validations)))
            f.close()
        elif(kwargs.get('load_validations')):
            print("Load validation dataset")
            f = h5py.File(str(kwargs['hdf5_path']), 'r')
            validations = load_data(f, u'validations')
            f.close()
        else:
            validations = []

        if(not kwargs.get('skip_pretest')):
            t1 = timeit.default_timer()
            # Create a lookup dictionary for our guesses
            total_attempts = 0
            attempts = 0
            correct = 0
            incorrect = 0
            missed = 0
            validation_guesses = { i: arr for (i, arr) in validations }
            for i, (index, row) in enumerate(test_df.iterrows()):
                total_topics = len(row['topics'])
                sorted_guesses = validation_guesses[index]
                guess_int = [None]*total_topics
                guess_word = [None]*total_topics
                guess_perc = [None]*total_topics
                for r in range(total_topics):
                    guess_int[r] = sorted_guesses[r:r+1][0][0]
                    guess_word[r] = topics_lb_enc.inverse_transform([guess_int[r]]).tolist()[0]
                    guess_perc[r] = sorted_guesses[r:r+1][0][1]
                    ## Tune here
                    # print(
                    #     "{0} - {1} : {2} @ {3:.2f} %".format(
                    #         r, guess_int[r], guess_word[r], guess_perc[r]*100
                    #     )
                    # )
                    ## highest: 53.20 correct % vs 46.80 incorrect  and 64 % missed (not guessed)
                    ## current: 42 / 58 with 38% missed
                    cutoff = 0.05  # cuttoff @ 0.1 => 31 / 69 with 0% missed

                    if((guess_word[r] in row['topics']) and (guess_perc[r]*100 > cutoff)):
                        correct += 1
                        attempts += 1
                    elif(guess_perc[r]*100 > cutoff):
                        incorrect += 1
                        attempts += 1
                    else:
                        missed += 1
                    total_attempts += 1

            print("RESULTS")
            print("---------------------------")
            print("Overall attempts: {}".format(attempts))
            print("Total correct: {}".format(correct))
            print("Total incorrect: {}".format(incorrect))
            print("Total missed: {}".format(missed))
            print(
                "Correct guesses: {0:.2f} % / {1:.2f} %".format(
                    (correct/attempts)*100, (correct/total_attempts)*100
                )
            )
            print(
                "Incorrect guesses: {0:.2f} % / {1:.2f} %".format(
                    (incorrect/attempts)*100, (incorrect/total_attempts)*100
                )
            )
            print(
                "Missed guesses (with respect to all possible attempts): {0:.2f} %".format(
                    (missed/total_attempts)*100
                )
            )
            t2 = timeit.default_timer()
            dtime = (t2 - t1)
            print("Evaluation time: {} seconds".format(dtime))
            print("Done.")
        else:
            print("Skipping pretesting....")
    else:
        print("No training csv file provided.")
        sys.exit(1)
    if('test_csv_path' in kwargs):
        test_csv_path = kwargs.get('test_csv_path')
        print(" Testing data: {}".format(test_csv_path))
        pred_df = load_dataframe(test_csv_path)
        # print(tabulate(pred_df.head(1)))
        # sys.exit(0)
        pred_df = preprocess_split_topics(pred_df)
        if(kwargs.get('rebuild_predictions')):
            print("Rebuilding predictions ")
            t1 = timeit.default_timer()
            
            print("Building prediction dataset")
            X_test = tfidf_transformer_tk.transform(cnt_vec.transform(pred_df['tokens']))
            X_test = scaler.transform(X_test)
            Xindex = list(pred_df.index)
            print("X_test shape: {}".format(X_test.shape))
            print("X_test type: {}".format(type(X_test)))
            print("X_test size: {0:.2f} Mb".format(X_test.data.nbytes / (1024*1024)))
            predictions = parallelize_predictions(clf, X_test, Xindex, data_partitions, cpu_count)
            store_sparse_matrix(X_test, 'X_test', kwargs['hdf5_path'])
            f = h5py.File(str(kwargs['hdf5_path']), 'a')
            store_data(f, u'predictions', predictions)
            print("Saved predictions: {} bytes".format(sys.getsizeof(predictions)))
            f.close()
            t2 = timeit.default_timer()
            dtime = (t2 - t1) / 60
            print("Test prediction time: {0:.2f} minutes".format(dtime))
        else:
            print("Load predictions dataset")
            f = h5py.File(str(kwargs['hdf5_path']), 'r')
            predictions = load_data(f, u'predictions')
            f.close()
            total_attempts = 0
            attempts = 0
            correct = 0
            incorrect = 0
            missed = 0
            prediction_guesses = { i: arr for (i, arr) in predictions }
            output_data = []
            for i, (index, row) in enumerate(pred_df.iterrows()):
                total_topics = len(row['topics'])
                sorted_guesses = prediction_guesses[index]
                guess_int = [None]*total_topics
                guess_word = [None]*total_topics
                guess_perc = [None]*total_topics
                guesses = []
                for r in range(total_topics):
                    guess_int[r] = sorted_guesses[r:r+1][0][0]
                    guess_word[r] = topics_lb_enc.inverse_transform([guess_int[r]]).tolist()[0]
                    guess_perc[r] = sorted_guesses[r:r+1][0][1]
                    cutoff = 0.05  # cuttoff @ 0.1 => 31 / 69 with 0% missed
                    if(guess_perc[r]*100 > cutoff):
                        guesses.append(guess_word[r])
                    
                output_data.append((index, guesses))
            print("Saving predictions...")
            with open('predictions.csv', 'w') as csvfile:
                pred_writer = csv.writer(
                    csvfile,
                    delimiter=',',
                    quoting=csv.QUOTE_MINIMAL
                )
                pred_writer.writerow(['', 'topics'])
                for (index, guesses) in output_data:
                    pred_writer.writerow(
                        [
                            index,
                            ','.join(guesses)
                            #     [str.encode(g,errors='ignore') for g in guesses]
                            # )
                        ]
                    )

    else:
        print("No testing csv file provided.")
        sys.exit(1)
    if('hdf5_path' not in kwargs):
        # print('hdf5_path: {}'.format(kwargs.get('hdf5_path')))
        kwargs['hdf5_path'] = '/tmp/lgr-default.hdf5'
    print('    hdf5_path: {}'.format(kwargs.get('hdf5_path')))


# Main entry into script.
if(__name__ == "__main__"):
    parser = ArgumentParser()
    parser.add_argument(
        "--train-file",
        dest="train_csv_path",
        help="Training input (.csv) file",
        metavar="FILE"
    )
    parser.add_argument(
        "--test-file",
        dest="test_csv_path",
        help="Testing input (.csv) file",
        metavar="FILE"
    )

    parser.add_argument(
        "--hdf5-file",
        dest="hdf5_path",
        metavar="FILE",
        help="Data storage path (.hdf5)"
    )

    parser.add_argument(
        '--rebuild-model',
        dest='rebuild_model',
        action='store_true',
    )

    parser.add_argument(
        '--rebuild-validations',
        dest='rebuild_validations',
        action='store_true'
    )

    parser.add_argument(
        '--load-validations',
        dest='load_validations',
        action='store_true'
    )

    parser.add_argument(
        '--skip-pretest',
        dest='skip_pretest',
        action='store_true'
    )

    parser.add_argument(
        '--rebuild-predictions',
        dest='rebuild_predictions',
        action='store_true',
    )

    args = parser.parse_args()
    kwargs = {
        'rebuild_model': args.rebuild_model,
        'rebuild_validations': args.rebuild_validations,
        'load_validations': args.load_validations,
        'skip_pretest': args.skip_pretest,
        'rebuild_predictions': args.rebuild_predictions,
    }
    if(args.train_csv_path):
        _tmp = plib.Path(args.train_csv_path).resolve()
        if(not _tmp.is_file()):
            print("Missing input file: '{}'".format(_tmp))
            sys.exit(1)
        kwargs['train_csv_path'] = _tmp
    if(args.test_csv_path):
        _tmp = plib.Path(args.test_csv_path).resolve()
        if(not _tmp.is_file()):
            print("Missing input file: '{}'".format(_tmp))
            sys.exit(1)
        kwargs['test_csv_path'] = _tmp
    ## setup our hdf5 storage
    if(args.hdf5_path):
        _tmp = plib.Path(args.hdf5_path).resolve()
    else:
        _tmp = plib.Path('/tmp/logistic-regression.hdf5')
    kwargs['hdf5_path'] = _tmp
    if(_tmp.is_file() and kwargs['rebuild_model']):
        replace_file = query_yes_no(
            "Do you want to replace the '{}' file?".format(
                _tmp.name
            ),
            default='no'
        )
        if(replace_file == 'no'):
            sys.exit(1)    

    main(**kwargs)
