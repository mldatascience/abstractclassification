import sys
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from skmultilearn.problem_transform import BinaryRelevance

if len(sys.argv) < 3:
    if rank == 0:
        print('Usage: python multinomialnb.py <train data file> <test data file>')
    sys.exit()

df_train = pd.read_csv(sys.argv[1], index_col=0)
df_test = pd.read_csv(sys.argv[2], index_col=0)

topic_list = set()
for i in df_train.index:
    topics = df_train['topics'][i].split(',')
    for topic in topics:
        if topic not in topic_list:
            topic_list.add(topic)
            df_train[topic] = 0    
        df_train.at[i, topic] = 1
topic_list = list(topic_list)

vect = CountVectorizer()
vect = vect.fit(df_train['tokens'])
train_x = vect.transform(df_train['tokens'])
test_x = vect.transform(df_test['tokens'])
train_y = df_train[topic_list]

clf = BinaryRelevance(MultinomialNB())
clf.fit(train_x, train_y)

pred = clf.predict_proba(test_x).toarray()

for df_i, pred_i in zip(df_test.index, range(len(pred))):
    topic_probs = []
    for topic_i in range(len(topic_list)):
        topic_probs.append((topic_i, pred[pred_i][topic_i]))
    topic_probs.sort(reverse=True, key=lambda x: x[1])
    num_topics = len(df_test['topics'][df_i].split(','))
    topics_str = ','.join([topic_list[x[0]] for x in topic_probs[:num_topics]])
    df_test.at[df_i, 'topics'] = topics_str

df_test.to_csv('predictions.csv', columns=['topics'])