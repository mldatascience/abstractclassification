from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn import linear_model
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
from collections import Counter


def predict_one(classifier, vectorizer, txt):
	subset=[txt]
	n=5
	testVect=vectorizer.transform(subset)
	probs=classifier.predict_proba(testVect)
	best_top_n=np.argsort(probs,axis=1)
	best_class_index=best_top_n[0:len(subset), len(classifier.classes_)-n: len(classifier.classes_)]
	best_classes=[classifier.classes_[a] for a in best_class_index[0]]
	best_classes=list(reversed(best_classes))
	c=','.join(str(e) for e in best_classes)
	return c






def train():
	filter_Size=100
	allText, allClass=getData2(filter_Size)
	#	X_train, X_test, y_train, y_test = train_test_split(allText, allClass, test_size=0.2, random_state=42)
	X_train_pack, X_test_pack, y_train_pack, y_test_pack = train_test_split(allText, allClass, test_size=0.1)
	X_train, y_train =unpack(X_train_pack, y_train_pack,filter_Size)
	vectorizer =TfidfVectorizer()
	X=vectorizer.fit_transform(X_train)
	classifier= linear_model.SGDClassifier(loss='log', penalty='elasticnet', max_iter=3,alpha=0.000005)
	#	classifier=OneVsRestClassifier(linear_model.SGDClassifier(loss='log', max_iter=2))
	classifier.fit(X, y_train)
#	joblib.dump(classifier,'classifier.pkl')
	#	calculateAccuracy( X_test_pack,  y_test_pack, classifier, vectorizer,0,6000)
	for i in range(0,1000):
		print("Batch: ",i)
		predict( classifier, vectorizer )

train()
