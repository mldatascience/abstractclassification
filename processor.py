import pandas as pd 
import numpy as np 
import networkx as nx 
from networkx.drawing.nx_agraph import write_dot
import os 

data=pd.read_csv('data/coursecat_stem_561.csv')

def distance(s1, s2):
    result=0
    set1=set(s1.split())
    set2=set(s2.split())
    setIntersection= set1.intersection(set2)
    result=len(setIntersection)
    return result

N=len(data)

matrix=np.zeros((N,N ))
for i in range(0, N):
    for j in range(i+1, N):
        matrix[i][j]=distance(data["stem"][i],data["stem"][j])

G=nx.Graph()
T=7
for i in range(0, N):
    G.add_node(data["CRSE_ID"][i])
    for j in range(i+1, N):
        if matrix[i][j]> T:
            G.add_edge(i, j)
print("nodes:", len( G.node), " edges:", len(G.edges))

write_dot(G, "course_network.dot") 

os.system("sfdp -Goverlap=prism course_network.dot -Tsvg > output.svg")









