import networkx as nx
import sys

if len(sys.argv) < 3:
    print('Usage: python dotfile_to_gpickle.py <input dotfile> <output gpickle file>')
    sys.exit()

graph = nx.drawing.nx_pydot.read_dot(sys.argv[1])
nx.write_gpickle(graph, sys.argv[2])
