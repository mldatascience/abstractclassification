import sys
import pandas as pd
import math

'''
Helper functions
'''
def get_lists(pred_df, test_df):
    for i in pred_df.index:
        pred = pred_df['topics'][i]
        test = test_df['topics'][i]
        yield pred.split(','), test.split(',')


def get_sets(pred_df, test_df):
    for pred_list, test_list in get_lists(pred_df, test_df):
        yield set(pred_list), set(test_list)


'''
Example-based evaluation metrics
'''
def exact_match_ratio(pred_df, test_df):
    ''' Exact Match Ratio
        The rate of exact matches between the prediction and test sets.
    '''
    exact_matches = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        if pred_set == test_set: exact_matches += 1
    return exact_matches / len(pred_df)


def accuracy(pred_df, test_df):
    ''' Accuracy
        The average proportion of correct predicted labels to total correct or incorrect labels.
    '''
    score_sum = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        score_sum += len(pred_set & test_set) / len(pred_set | test_set)
    return score_sum / len(pred_df)


def precision(pred_df, test_df):
    ''' Precision
        The average proportion of correct predicted labels to total correct labels.
    '''
    score_sum = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        score_sum += len(pred_set & test_set) / len(test_set)
    return score_sum / len(pred_df)


def recall(pred_df, test_df):
    ''' Recall
        The average proportion of correct predicted labels to total predicted labels.
    '''
    score_sum = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        score_sum += len(pred_set & test_set) / len(pred_set)
    return score_sum / len(pred_df)


def f1_measure(pred_df, test_df):
    ''' F1 Measure
        The harmonic mean of Precision and Recall.
    '''
    score_sum = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        score_sum += (2 * len(pred_set & test_set)) / (len(pred_set) + len(test_set))
    return score_sum / len(pred_df)


def hamming_loss(pred_df, test_df):
    ''' Hamming Loss
        The average number of total missing or incorrect labels,
        normalized over the total number of possible labels.
    '''
    all_labels = set()
    score_sum = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        all_labels |= (pred_set | test_set)
        score_sum += len(pred_set - test_set) + len(test_set - pred_set)
    return score_sum / (len(pred_df)*len(all_labels))

'''
Label-based evaluation metrics
'''
# def alpha_evaluation(pred_df, test_df):
#     pass


'''
Ranking-based evaluation metrics
'''
def one_error(pred_df, test_df):
    ''' One-Error
        The rate at which the top-ranked label is incorrect.
    '''
    score_sum = 0
    for pred_list, test_list in get_lists(pred_df, test_df):
        if pred_list[0] not in test_list: score_sum += 1
    return score_sum / len(pred_df)

# def coverage(pred_df, test_df):
#     pass

# def ranking_loss(pred_df, test_df):
#     pass

# def average_precision(pred_df, test_df):
#     pass


'''
Distance-based evaluation metrics
'''
def r_accuracy(pred_df, test_df, gpickle_file):
    # only import networkx if needed
    import networkx as nx
    graph = nx.read_gpickle(gpickle_file)

    score_sum = 0
    invalid_rows = 0
    for pred_set, test_set in get_sets(pred_df, test_df):
        # only consider labels on the graph
        pred_set = set(label for label in pred_set if graph.has_node(label))
        test_set = set(label for label in test_set if graph.has_node(label))
        if not len(pred_set) or not len(test_set):
            # do not count rows with no labels from the graph
            invalid_rows += 1
            continue
        
        inner_sum = 0
        for pred_label in pred_set:
            # find the shortest distance to a test label
            min_dist = math.inf
            for test_label in test_set:
                try:
                    distance = nx.shortest_path_length(graph, pred_label, test_label)
                except nx.NetworkXNoPath:
                    distance = math.inf
                min_dist = min(min_dist, distance)
            # scale distance logistically
            inner_sum += math.e**(-min_dist)
        
        score_sum += inner_sum / len(pred_set)
    
    if invalid_rows < len(pred_df):
        return score_sum / (len(pred_df) - invalid_rows)
    else:
        return 0


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: python evaluate_predictions.py <prediction dataset> <test dataset> [<label graph gpickle file>]')
        sys.exit()
    
    pred_df = pd.read_csv(sys.argv[1], index_col=0)
    test_df = pd.read_csv(sys.argv[2], index_col=0)

    pred_df = pred_df.dropna()

    print('\nEvaluating {} examples'.format(len(pred_df)))

    print('\nEXAMPLE-BASED METRICS')
    print('Exact Match Ratio: {:.4f}'.format(exact_match_ratio(pred_df, test_df)))
    print('Accuracy:          {:.4f}'.format(accuracy(pred_df, test_df)))
    print('Precision:         {:.4f}'.format(precision(pred_df, test_df)))
    print('Recall:            {:.4f}'.format(recall(pred_df, test_df)))
    print('F1 Measure:        {:.4f}'.format(f1_measure(pred_df, test_df)))
    print('Hamming Loss:      {:.4f}'.format(hamming_loss(pred_df, test_df)))

    if (len(sys.argv) > 3):
        use_distance = input('\nUse label graph for distance-based evaluation? (y/n) ')
        if use_distance.lower().startswith('y'):
            print('Loading label graph...')
            print('\nDISTANCE-BASED METRICS\nR-Accuracy: {:.4f}'.format(r_accuracy(pred_df, test_df, sys.argv[3])))

    is_ranked = input('\nAre the prediction labels in rank order? (y/n) ')

    if is_ranked.lower().startswith('y'):
        print('\nRANKING-BASED METRICS')
        print('One-Error: {:.4f}'.format(one_error(pred_df, test_df)))
