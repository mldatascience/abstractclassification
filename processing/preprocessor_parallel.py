from mpi4py import MPI
import pandas as pd
from npextractor import get_continuous_chunks
from nltk import RegexpParser
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from timeit import default_timer as timer
ps=PorterStemmer()

filename="../data/exportingForML_full.csv"

#numberofrec=1000
thresold=3
df=pd.read_csv(filename)
NP = "NP: {(<V\w+>|<NN\w?>)+.*<NN\w?>}"
chunker = RegexpParser(NP)
#df=df[0:numberofrec]

#get MPI communication 

comm=MPI.COMM_WORLD
rank =comm.Get_rank()
nprocs=comm.Get_size()


recperrank=int(len(df)/(nprocs-1))
#run to every slave
if rank>0:
	outfilename="nodeoutput/npstem_rank"+str(rank)+".csv"
	df=df[(rank-1)*recperrank:rank*recperrank].reset_index()
	df['text']=df['title'].astype(str) + " " + df["abs"].astype(str) 
	df['np']=df['text'].apply(lambda sent: get_continuous_chunks(sent, chunker.parse))
	stemtxt=[]

	for i in range(0, len(df)):
		txt=' '.join(df['np'][i]).split(" ")
		stext= [ps.stem(t) for t in txt ]
		stext=[ k for k in stext if len(k)>thresold ]
		a=' '.join(stext)
		stemtxt.append(a)
		if i % 500:
			print("rank:" ,rank, " progress " , i , "out of ", len(df) , " \r", end='')

	df['stem']=stemtxt	
	savestemdf=df[['gtopics','stem']]
	
	savestemdf.to_csv(outfilename,index=False)
	print("rank:" ,rank, " Done processing. File saved:", outfilename )	 
	#import pdb; pdb.set_trace()
	comm.send(savestemdf, dest=0)

#run only for master
if rank==0:
	data=comm.recv(source=1)
	for i in range(2, nprocs):
		di=comm.recv(source=i)
		data=data.append(di)
	data.to_csv('np_master.csv', index=False)
	

 


