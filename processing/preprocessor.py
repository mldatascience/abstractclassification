import pandas as pd
from npextractor import get_continuous_chunks
from nltk import RegexpParser
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
ps=PorterStemmer()

#nltk.download('punkt')
#nltk.download('averaged_perceptron_tagger')

filename="../data/exportingForML_full.csv"

numberofrec=1000
thresold=3
df=pd.read_csv(filename)
NP = "NP: {(<V\w+>|<NN\w?>)+.*<NN\w?>}"
chunker = RegexpParser(NP)




df=df[0:numberofrec]
df['text']=df['title'].astype(str) + " " + df["abs"].astype(str) 
df['np']=df['text'].apply(lambda sent: get_continuous_chunks(sent, chunker.parse))
stemtxt=[]

for i in range(0, len(df)):
	txt=' '.join(df['np'][i]).split(" ")
	stext= [ps.stem(t) for t in txt ]
	stext=[ k for k in stext if len(k)>thresold ]
	a=' '.join(stext)
	stemtxt.append(a)
	if i % 500:
		print("progress " , i , "out of ", numberofrec , " \r", end='')

df['stem']=stemtxt	
savestemdf=df[['gtopics','stem']]
outfilename="npstem"+str(numberofrec)+".csv"
savestemdf.to_csv(outfilename,index=False)
print("Done processing. File saved:", outfilename )	 
#import pdb; pdb.set_trace()



