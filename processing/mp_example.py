import pandas as pd
from npextractor import get_continuous_chunks
from nltk import RegexpParser
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
ps=PorterStemmer()
NP = "NP: {(<V\w+>|<NN\w?>)+.*<NN\w?>}"
chunker = RegexpParser(NP)

data="According to the American Association of Nurse Practitioners (AANP 2016), nurse practitioners (NPs) are licensed, autonomous clinicians focused on managing peoples health conditions and preventing disease. As advanced practice registered nurses (APRNs), NPs often specialize by patient population, including pediatric, adult-gerontological, and womens health. NPs may also subspecialize in areas such as dermatology, cardiovascular health, and oncology."

data='''
He is in his office. He sat under a tree. There is someone at the door. The crocodiles snapped at the boat. Put the books on the table.
There are many apples on the tree.
A gang stood in front of me.
The castle was heavily bombed during the war.'''


txt=get_continuous_chunks(data, chunker.parse)

print(txt)
