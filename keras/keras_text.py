import pandas as pd
import keras.preprocessing.text as text
import sklearn
from sklearn.preprocessing import LabelEncoder
from sklearn import preprocessing
import json

import numpy as np

data=pd.read_csv("../data/exportingForML_full.csv",  dtype={'title': object, 'abs':object})
data["text"] =   data["title"].map(str) +  " " +  data["abs"].map(str)
vocab_size=50000
tokenize = text.Tokenizer(num_words=vocab_size)
'''
data["classes"]
cls=[]
for i in range(0, len(data)):
    c=json.loads(data['gtopics'][i])
    print(c[0]['refinedStemmedPhrases'])
#    cls.append(c[0]['refinedStemmedPhrases'])

'''


x= [ json.loads(data['gtopics'][i])[0]['refinedStemmedPhrases'] for i in range(0, len(data))]

train_size = int(len(data) * .8)
train_abs = data['text'][:train_size]
train_tags = data['gtopics'][:train_size]
test_abs = data['text'][train_size:]
test_tags = data['gtopics'][train_size:]


#tokenize.fit_on_texts(train_abs)
tokenize.fit_on_texts(data['text'])
x_data = tokenize.texts_to_matrix(data['text'])
#s=str(x_data)
#np.savetxt('matrix.csv', x_data, delimiter=',')
#f=open("matrix.csv","w")
#f.write()
'''

encoder = preprocessing.LabelBinarizer()
encoder.fit(train_tags)
y_train = encoder.transform(train_tags)
y_test = encoder.transform(test_tags)

'''
