import networkx as nx
import sys

graph = nx.drawing.nx_pydot.read_dot(sys.argv[1])


components = list(nx.connected_component_subgraphs(graph))

largest = max(components, key=lambda x: nx.number_of_nodes(x))

nx.drawing.nx_pydot.write_dot(largest, 'largest.dot')

