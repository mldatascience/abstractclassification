#!/usr/bin/env python

"""
Import csv file from command

./extract_train_test_set.py
"""


import argparse
import os
import pandas as pd
from sklearn.utils import shuffle as sk_shuffle


def main(input_file, percent, random_state):
    """
    Import csv from commandline, returns two output files
    """
    # print("Extracting data...")
    # print('Input file: {}'.format(input_file))
    # print('Percent: {0:.2f}% test data size'.format(percent*100))
    # print('Random state: {}'.format(random_state))

    df = pd.read_csv(input_file)
    df = sk_shuffle(df, random_state=random_state).reset_index(drop=True)

    test_size = 0.25
    train_size = 1 - test_size
    test_rec_size = round(len(df)*test_size)
    train_rec_size = round(len(df)*train_size)

    if(test_rec_size + train_rec_size) != len(df):
        raise Exception("Dataset size missmatch.. This shouldn't happen...")
    train_set = df[0:train_rec_size]
    test_set = df[train_rec_size:]

    output_file_path = os.path.splitext(input_file)[0]
    output_file_train = output_file_path + '_train.csv'
    output_file_test = output_file_path + '_test.csv'

    train_set.to_csv(output_file_train)
    test_set.to_csv(output_file_test)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help='input csv data file')
    parser.add_argument(
        '-p',
        '--percent',
        dest='percent',
        default=0.25,
        type=float,
        help="Test data set size in percent"
    )
    parser.add_argument(
        '-r',
        '--random-state',
        dest='random_state',
        default=1625,
        type=int,
        help="random state"
    )
    args = parser.parse_args()
    main(args.input_file, args.percent, args.random_state)
