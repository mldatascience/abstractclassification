from mpi4py import MPI
import sys

from .tokenize_abstracts import load_csv, parse_topic_lists, tokenize_text, token_count_cutoff

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
cpus = comm.Get_size()
print(f'{rank}: started')

if len(sys.argv) < 3:
    if rank == 0:
        print('Usage: python tokenize_parallel.py <source file> <dest file>')
    sys.exit()

if rank == 0:
    print('0: reading CSV')
    df = load_csv(sys.argv[1])

    for i in range(1, cpus):
        comm.send(df, dest=i)


if rank > 0:
    df = comm.recv(source=0)

    rows_per_cpu = int(len(df)/(cpus-1))
    df = df[(rank-1)*rows_per_cpu:rank*rows_per_cpu]
    df.index = range(len(df))

    df['topics'] = parse_topic_lists(df)
    df = df[df.topics != '']
    df['tokens'] = tokenize_text(df)
    df = token_count_cutoff(df, 20, 60)

    print(f'{rank}: finished')
    df_out = df[['topics','tokens']]
    comm.send(df_out, dest=0)


if rank == 0:
    data = comm.recv(source=1)
    for i in range(2, cpus):
        di = comm.recv(source=i)
        data = data.append(di)
    
    print('0: writing CSV')
    data.to_csv(sys.argv[2], index=False)
    print('0: finished')
