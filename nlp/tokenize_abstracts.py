import re
import sys
import pandas as pd
import spacy
from nltk.stem import PorterStemmer
from bs4 import BeautifulSoup
import json


def load_csv(filename):
    df = pd.read_csv(
        filename,
        converters={
            'gtopics': json.loads,
        },
        header=0,
    )
    df = df.fillna(value='')
    return df


def parse_topic_lists(df):

    def extract_topics(gtopics):
        topics = []
        for d in gtopics:
            if 'refinedStemmedPhrases' in d:
                topics += d['refinedStemmedPhrases'].split(',')
        return ','.join([ x for x in list(set(sorted(topics))) if x ])

    return df['gtopics'].apply(extract_topics)


def tokenize_text(df):

    nlp = spacy.load('en_core_web_sm')
    stemmer = PorterStemmer()
    full_texts = df['title'].astype(str) + " " + df['abs'].astype(str)

    def tokenize(text):
        text = BeautifulSoup(text, 'html.parser').get_text()
        text = re.sub('\([A-Z0-9]+\w?\)', '', text)
        doc = nlp(text)

        filtered = []
        for token in doc:
            if (token.is_ascii and token.is_alpha and 
                    not token.is_stop and token.ent_type == 0):
                filtered.append(token.lower_)

        stems = [stemmer.stem(word) for word in filtered]
        filtered_stems = [stem for stem in stems if len(stem) > 2]
        
        return ' '.join(filtered_stems)
    
    return full_texts.apply(tokenize)


def token_count_cutoff(df, min_tokens, max_tokens):

    def count_tokens(abstract):
        a = str(abstract)
        if len(a) == 0: return 0
        return len(a.split(' '))
    
    df['counts'] = df['tokens'].apply(count_tokens)
    return df[df.counts >= min_tokens][df.counts <= max_tokens]


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: python tokenize.py <source file> <dest file>')
        sys.exit()
    
    df = load_csv(sys.argv[1])

    df['topics'] = parse_topic_lists(df)
    df = df[df.topics != '']
    df['tokens'] = tokenize_text(df)
    df = token_count_cutoff(df, 20, 60)

    df.to_csv(sys.argv[2], columns=['topics', 'tokens'], index=False)
