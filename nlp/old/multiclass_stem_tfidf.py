import re
import csv
import sys
import pandas as pd
import nltk
import string
import swifter
import numpy as np
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

if len(sys.argv) < 3:
    print('Usage: python multiclass_stem_tfidf.py <source file> <dest file>')
    sys.exit()

# check for required NLTK packages
try:
    nltk.data.find('tokenizers/punkt')
    nltk.data.find('corpora/stopwords')
    nltk.data.find('corpora/wordnet')
    nltk.data.find('corpora/words')
    nltk.data.find('taggers/averaged_perceptron_tagger')
    nltk.data.find('chunkers/maxent_ne_chunker')
except LookupError:
    nltk.download('punkt')
    nltk.download('stopwords')
    nltk.download('averaged_perceptron_tagger')
    nltk.download('wordnet')
    nltk.download('maxent_ne_chunker')
    nltk.download('words')

df = pd.read_csv(sys.argv[1])
# Drop null and duplicate rows
df = df.dropna()
df = df.drop_duplicates(subset=['abs'])

print('Parsing research topics...')
def get_topic_list(gtopics):
    topics = re.search('",(.+),"', gtopics)
    if topics: return topics.group(1)
    else: return ''
df['topics'] = df['gtopics'].swifter.apply(get_topic_list)

# Drop rows with no topics
df = df[df.topics != '']
df.index = range(len(df))

print('Tokenizing abstracts...')
abstracts = df['abs'].swifter.apply(lambda x: BeautifulSoup(x, 'html.parser').get_text())
abstracts = abstracts.swifter.apply(lambda x: re.sub('\([A-Z0-9]+\w?\)', '', x))
abstracts = abstracts.swifter.apply(nltk.tokenize.word_tokenize)

# Filter out stop words, tokens with non-ASCII characters, name entities, and punctuation
print('Filtering tokens...')
stop_words = set(nltk.corpus.stopwords.words('english'))
def filter_tokens(tokens):
    name_entities = set()
    ne_tree = nltk.ne_chunk(nltk.pos_tag(tokens))
    for chunk in ne_tree:
        if hasattr(chunk, 'label'):
            for token in chunk:
                name_entities.add(token[0])
    
    filtered = []
    for token in tokens:
        try:
            token.encode(encoding='utf-8').decode('ascii')
        except UnicodeDecodeError:
            continue
        try:
            float(token)
            continue
        except ValueError:
            pass
        if token in stop_words:
            continue
        if token in name_entities:
            continue
        if token in string.punctuation:
            continue
        filtered.append(re.sub('[0-9]+', '', token).lower())
    return filtered
abstracts = abstracts.swifter.apply(filter_tokens)

print('Stemming tokens...')
stemmer = nltk.stem.porter.PorterStemmer()
abstracts = abstracts.swifter.apply(lambda x: [stemmer.stem(word) for word in x])
abstracts = abstracts.swifter.apply(lambda x: [stem for stem in x if len(stem) > 2])

print('Analyzing token frequencies...')
cvec = CountVectorizer()
concatenated = abstracts.swifter.apply(' '.join)
token_counts = np.array(cvec.fit_transform(concatenated).toarray()).sum(axis=0)
# Filter out tokens with >1 occurrence
sig_tokens = set()
for token, count in zip(cvec.get_feature_names(), token_counts):
    if count > 1:
        sig_tokens.add(token)
abstracts = abstracts.swifter.apply(lambda x: [token for token in x if token in sig_tokens])

print('Calculating TF-IDF weights...')
tvec = TfidfVectorizer()
concatenated = abstracts.swifter.apply(' '.join)
weights = tvec.fit_transform(concatenated).toarray()

with open(sys.argv[2], 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['TOPICS'] + tvec.get_feature_names())
    for i in range(len(weights)):
        writer.writerow([df['topics'][i]] + list(weights[i]))

print('\nCreated {}x{} TF-IDF matrix.'.format(*weights.shape))
print('Saved output to {}'.format(sys.argv[2]))


# Parse topic lists
# topics = df['gtopics'].apply(lambda x: re.search('",(.+),"', x).group(1))

# Calculate term frequencies
# count_matrix = np.array(cvec.fit_transform(abstracts).toarray())
# tf_matrix = count_matrix / np.expand_dims(count_matrix.sum(axis=1), axis=1)

# Export to CSV
# with open(sys.argv[2], 'w') as csvfile:
#     writer = csv.writer(csvfile)
#     writer.writerow(['TOPIC'] + cvec.get_feature_names())
#     for i in range(len(tf_matrix)):
#         writer.writerow([df['topic'][i]] + list(tf_matrix[i]))

# print('\nCreated {}x{} TF matrix.'.format(*tf_matrix.shape))
# print('Saved output to {}'.format(sys.argv[2]))