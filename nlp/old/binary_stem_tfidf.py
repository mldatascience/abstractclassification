import re
import csv
import sys
import pandas as pd
import nltk
import string
import swifter
import numpy as np
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

if len(sys.argv) < 3:
    print('Usage: python binary_stem_tfidf.py <source file> <dest file>')
    sys.exit()

# check for required NLTK packages
try:
    nltk.data.find('tokenizers/punkt')
    nltk.data.find('corpora/stopwords')
except LookupError:
    nltk.download('punkt')
    nltk.download('stopwords')

print('Reading CSV data...')
df = pd.read_csv(sys.argv[1])

# Drop null and duplicate rows
df = df.dropna()
df = df.drop_duplicates(subset=['abs'])
df.index = range(len(df))

# Categorize rows based on 'machine learning' and 'cancer'
def get_topic(gtopics):
    if 'cancer' in gtopics and 'machine learning' not in gtopics: return 0
    if 'machine learning' in gtopics and 'cancer' not in gtopics: return 1
    return -1
df['topic'] = df['gtopics'].swifter.apply(get_topic)

# Remove rows that have both or neither
df = df[df.topic != -1]
df.index = range(len(df))

print('Tokenizing abstracts...')
abstracts = df['abs'].swifter.apply(lambda x: BeautifulSoup(x, 'html.parser').get_text())
abstracts = abstracts.swifter.apply(lambda x: re.sub('\([A-Z0-9]+\w?\)|[0-9]+', '', x))
abstracts = abstracts.swifter.apply(lambda x: x.lower())
abstracts = abstracts.swifter.apply(nltk.tokenize.word_tokenize)

# Filter out stop words, tokens with non-ASCII characters, and punctuation
stop_words = set(nltk.corpus.stopwords.words('english'))
def filter_tokens(tokens):
    filtered = []
    for token in tokens:
        try:
            token.encode(encoding='utf-8').decode('ascii')
        except UnicodeDecodeError:
            continue
        if token in stop_words:
            continue
        if token in string.punctuation:
            continue
        filtered.append(token)
    return filtered
abstracts = abstracts.swifter.apply(filter_tokens)

# Stem tokens
stemmer = nltk.stem.porter.PorterStemmer()
abstracts = abstracts.swifter.apply(lambda x: [stemmer.stem(word) for word in x])

print('Analyzing token frequencies...')
cvec = CountVectorizer()
concatenated = abstracts.swifter.apply(' '.join)
token_counts = np.array(cvec.fit_transform(concatenated).toarray()).sum(axis=0)
# Filter out tokens with >1 occurrence
sig_tokens = []
for token, count in zip(cvec.get_feature_names(), token_counts):
    if count > 1:
        sig_tokens.append(token)
abstracts = abstracts.swifter.apply(lambda x: [token for token in x if token in sig_tokens])

print('Calculating TF-IDF weights...')
tvec = TfidfVectorizer()
concatenated = abstracts.swifter.apply(' '.join)
weights = tvec.fit_transform(concatenated).toarray()

with open(sys.argv[2], 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['TOPIC'] + tvec.get_feature_names())
    for i in range(len(weights)):
        writer.writerow([df['topic'][i]] + list(weights[i]))

print('\nCreated {}x{} TF-IDF matrix.'.format(*weights.shape))
print('Saved output to {}'.format(sys.argv[2]))
