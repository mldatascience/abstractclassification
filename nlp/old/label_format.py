import re
import csv
import sys
import pandas as pd
from bs4 import BeautifulSoup


if len(sys.argv) < 3:
    print('Usage: python label_format.py <source file> <dest file>')
    sys.exit()

df = pd.read_csv(sys.argv[1])
df = df.dropna()
df = df.drop_duplicates(subset=['abs'])
df.index = range(len(df))

def get_labels(gtopics):
    topics = re.search('",(.+),"', gtopics)
    if not topics: return ''
    topic_list = topics.group(1).split(',')
    label_list = []
    for topic in topic_list:
        topic = topic.replace(' ', '-')
        label_list.append(f'__label__{topic}')
    return ' '.join(label_list)

def parse_html(abstract):
    return BeautifulSoup(abstract, 'html.parser').get_text()

with open(sys.argv[2], 'w') as file:
    for i in range(len(df)):
        file.write('{} {} {}\n'.format(
            get_labels(df['gtopics'][i]),
            parse_html(df['title'][i]),
            parse_html(df['abs'][i])
        ))
