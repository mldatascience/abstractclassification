from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize

ps = PorterStemmer() 
  
# choose some words to be stemmed 
words = ["program", "programs", "programer", "programing", "programers"] 
  
for w in words: 
    print(w, " : ", ps.stem(w)) 

   

def processdata(data):
    result=""
    ps = PorterStemmer()
    words = word_tokenize(data) 
    result=[]
    for  d in words:
        if len(d)>3: result.append(ps.stem(d))

    # write code
    return result
data="Digital Africana Studies: The Harlem Renaissance,    Digital Africana Studies aims to bridge the best of Africana Studies (key concepts, theories, methods of inquiry, and pedagogies) with the democratic potential of Digital Humanities. Digital Africana Studies examines and re-imagines possibilities for the practices and structural logics of Digital Humanities and digital media broadly by questioning the often taken-for-granted assumptions of Digital Humanities spaces, discourses and cultural productions. To the degree that Africana Studies has long advocated for the inclusion of African American contributions and the documenting of historical racial struggles for diversity and social justice, Digital Africana Studies encourages critical yet productive engagements through literature, art, history and popular culture."
print(processdata(data))

