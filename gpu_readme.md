This readme describe how to use GPU using pycuda in UA HPC system

# one time task: download gpu enabled singularity/docker image #
```shell
module load singularity
singularity pull --name gdocker.simg docker://hossain/gdocker
```

# runing jobs #
### load modules ###

```shell
module load cuda91/toolkit/9.1.85
module load singularity
```
  
### run a interactive job (example) ###
```shell
qsub -I -N gpu_test -W group_list=kobourov -q standard -l select=1:ncpus=28:mem=168gb:ngpus=1 -l cput=1:0:0 -l walltime=1:0:0
```

### run singularity container ###
```shell
singularity shell --nv  gdocker.simg
```
###  these two env variables should present in gdocker (No need to run) ###
```shell
export NUMBAPRO_NVVM=/cm/shared/apps/cuda91/toolkit/9.1.85/nvvm/lib64/libnvvm.so
export NUMBAPRO_LIBDEVICE=/cm/shared/apps/cuda91/toolkit/9.1.85/nvvm/libdevice/
```

### run code  ###
```shell
python3 yourcode.py
```

# add this line in PBS script
```shell
singularity exec --nv  gdocker.simg  python3 yourcode.py
```
